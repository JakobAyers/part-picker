class CPU:

    def __init__(self, core, base, boost, overclock, brand, thread,\
           graphics, name):
        self.core = core
        self.base = base
        self.boost = boost
        self.overclock = overclock
        self.brand = brand
        self.thread = thread
        self.graphics = graphics
        self.name = name

i9_9900ks = CPU(8, 4.00, 5.00, True, 'intel', 16, True, 'i9 9900ks')
i9_9900k = CPU(8, 3.60, 5.00, True, 'intel', 16, True, 'i9 9900k')
i9_9900kf = CPU(8, 3.60, 5.00, True, 'intel', 16, False, 'i9 9900kf')
i9_9900 = CPU(8, 3.10, 5.00, False, 'intel', 16, True, 'i9 9900')

i7_9700k = CPU(8, 3.60, 4.90, True, 'intel', 8, True, 'i7 9700k')
i7_9700kf = CPU(8, 3.60, 4.90, True, 'intel', 8, False, 'i7 9700kf')
i7_9700 = CPU(8, 3.00, 4.70, False, 'intel', 8, True, 'i7 9700')
i7_9700f = CPU(8, 3.00, 4.70, False, 'intel', 8, False, 'i7 9700f')

i5_9600k = CPU(6, 3.70, 4.60, True, 'intel', 6, True, 'i5 9600k')
i5_9600kf = CPU(6, 3.70, 4.60, True, 'intel', 6, False, 'i5 9600kf')
i5_9600 = CPU(6, 3.10, 4.60, False, 'intel', 6, True, 'i5 9600')
i5_9500 = CPU(6, 3.00, 4.40, False, 'intel', 6, True, 'i5 9500')
i5_9500f = CPU(6, 3.00, 4.40, False, 'intel', 6, False, 'i5 9500f')
i5_9400 = CPU(6, 2.90, 4.10, False, 'intel', 6, True, 'i5 9400')
i5_9400f = CPU(6, 2.90, 4.10, False, 'intel', 6, False, 'i5 9400f')

i3_9350kf = CPU(4, 4.00, 4.60, True, 'intel', 4, False, 'i3 9300kf')
i3_9320 = CPU(4, 3.70, 4.40, False, 'intel', 4, True, 'i3 9320')
i3_9300 = CPU(4, 3.70, 4.30, False, 'intel', 4, True, 'i3 9300')
i3_9100 = CPU(4, 3.60, 4.20, False, 'intel', 4, True, 'i3 9100')
i3_9100f = CPU(4, 3.60, 4.20, False, 'intel', 4, False, 'i3 9100f')


ry3_3100 = CPU(4, 3.60, 3.90, True, 'amd', 8, False, 'ryzen 3 3100')
ry3_3300x = CPU(4, 3.80, 4.30, True, 'amd', 8, False, 'ryzen 3 3200x')

ry5_3600 = CPU(6, 3.60, 4.20, True, 'amd', 12, False, 'ryzen 5 3600')
ry5_3600x = CPU(6, 3.80, 4.40, True, 'amd', 12, False, 'ryzen 5 3600x') 

ry7_3700x = CPU(8, 3.60, 4.40, True, 'amd', 16, False, 'ryzen 7 3700x')
ry7_3800x = CPU(8, 3.90, 4.50, True, 'amd', 16, False, 'ryzen 7 3800x')

ry9_3900x = CPU(12, 3.80, 4.60, True, 'amd', 24, False, 'ryzen 9 3900x')
ry9_3950x = CPU(16, 3.50, 4.70, True, 'amd', 32, False, 'ryzen 9 3950x')

cpus = [i9_9900ks, i9_9900k, i9_9900kf, i9_9900, i7_9700k, i7_9700kf, i7_9700, \
        i7_9700f, i5_9600k, i5_9600kf, i5_9600, i5_9500, i5_9500f, i5_9400, \
        i5_9400f, i3_9350kf, i3_9320, i3_9300, i3_9100, i3_9100f, ry3_3100, \
        ry3_3300x, ry5_3600, ry5_3600x, ry7_3700x, ry7_3800x, ry9_3900x, \
        ry9_3950x]

def getcpu(core, base, boost, overclocking, brand, graphics):
    
    if brand == 'amd' or brand == 'AMD':
        if graphics == True:
            return ('graphicserror')
        
        elif core == 4:
            if boost > 4.00:
                return (ry3_3300x)
            else:
                return (ry3_3100)
        
        elif core == 6:
            if boost > 4.30:
                return (ry5_3600)
            else:
                return (ry5_3600x)
        
        elif core == 8:
            if base > 4.40:
                return (ry7_3700x)
            else:
                return (ry7_3800x)

        elif core == 10 or core == 12 or core == 11:
            if base > 3.90:
                return (ry7_3800x)
            else:
                return (ry9_3900x)
        
        elif core == 14 or core == 15 or core == 16:
            if base > 3.80:
                return (ry9_3900x)
            else:
                return (ry9_3950x)

    elif brand == 'intel' or brand == 'Intel':
        if overclocking == True and graphics == False and core == 4:
            return (i3_9350kf)
        elif overclocking == True and graphics == True and core == 4:
            return (i5_9600k)
        
        elif core == 4:
            if base > 3.60:
                if boost > 4.30:
                    return (i3_9320)
                else:
                    return (i3_9300)
        
            elif 3.60 >= base and graphics == True:
                return (i3_9100)
            elif 3.60 >= base and graphics == False:
                return (i3_9100f)
        
        elif core -- 5 or core == 6:
            
            if overclocking == True:
                if graphics == True:
                    return (i5_9600k)
                if graphics == False:
                    return (i5_9600kf)
                
            if overclocking == False:
                if boost > 4.40:
                    return (i5_9600)
                
                elif boost > 4.20:
                    if graphics == True: 
                        return (i5_9500)
                    elif graphics == False: 
                        return (i5_9500f)
                
                elif boost <= 4.20: 
                    if graphics == True:
                        return (i5_9400)
                    if graphics == False:
                        return (i5_9400f)
        
        elif core >=7:
            if boost == 5:
            
                if base > 3.60: 
                    return (i9_9900ks)
                
                elif base > 3.1: 
                    if graphics == True:
                        return (i9_9900k)
                    elif graphics == False:
                        return (i9_9900kf)
                
                elif base <= 3.10:
                    return (i9_9900)
            
            elif boost > 4.70:
                if overclocking == True:
                    if graphics == True:
                        return (i7_9700k)
                    if graphics == False:
                        return (i7_9700kf)
            
            elif boost <= 4.70:
                if graphics == True:
                    return (i7_9700)
                if graphics == False:
                    return (i7_9700f)
    else:
        return('branderror')

def getcpuinfo(core, base, boost, overclocking, brand, graphics):
    
    cpu = getcpu(core, base, boost, overclocking, brand, graphics)
    print (cpu)    
    
    if cpu == 'branderror':
        return ('branderror')
    if cpu =='graphicserror':
        return('graphicserror')

    lines = {cpu.name, f'{cpu.core} cores, {cpu.thread} threads,\
 base clock of {cpu.base}GHz, boost clock of {cpu.boost}GHz'}
    return (lines)
