from tkinter import *
from tkinter import messagebox
from pickpart import getcpu
from pickpart import getcpuinfo

def getdata():

    lines = getcpuinfo(corevar.get(), basevar.get(), boostvar.get(), \
            overclockvar.get(), brandentry.get(),graphicsvar.get())
    print(lines)
    if lines == 'branderror':
        messagebox.showerror('Error', 'Please enter either AMD or Intel in the\
                brand entry box.')
    elif lines == 'graphicserror':
        messagebox.showerror('Error', 'The CPU list does not include AMD APUs\
 so none of the AMD CPUs have graphics.')

    else:
        messagebox.showinfo('CPU', "\n".join(lines))

window = Tk()
window.title('part picker')



cores = Label(window, text = 'Cores:')
cores.grid(row = 0, column = 0, sticky = 'E')

corevar = DoubleVar()

coreslide = Scale(window, orient = HORIZONTAL, from_ = 4, to = 16, \
        variable = corevar)
coreslide.grid(row = 0, column = 1, sticky = 'W')



base = Label(window, text = 'Base Clock:')
base.grid(row = 1, column = 0, sticky = 'E')

basevar = DoubleVar()

baseslide = Scale(window, orient = HORIZONTAL, from_ = 2.9, to = 4, \
        resolution = .1, variable = basevar)
baseslide.grid (row = 1, column = 1, sticky = 'W')



boost = Label(window, text = 'Boost Clock:')
boost.grid(row = 3, column = 0, sticky = 'E')

boostvar = DoubleVar()

boostslide = Scale(window, orient = HORIZONTAL, from_ = 3.9, to = 5, \
        resolution = .1, variable = DoubleVar)
boostslide.grid(row = 3, column = 1, sticky = 'W')



graphics = Label(window, text = 'Graphics:')
graphics.grid(row = 4, column = 0, sticky = 'E')

graphicsvar = IntVar()

graphicsbox = Checkbutton(window, variable = graphicsvar)
graphicsbox.grid (row = 4, column = 1, sticky = 'W')



overclock = Label(window, text = 'Overclockable:')
overclock.grid(row = 5, column = 0, sticky = 'E')

overclockvar = IntVar()

overclockbox = Checkbutton(window, variable = overclockvar)
overclockbox.grid(row = 5, column = 1, sticky = 'W')



brand = Label(window, text = 'AMD or Intel:')
brand.grid(row = 6, column = 0, sticky = 'E')

brandentry = Entry(window)
brandentry.grid(row = 6, column = 1, sticky = 'W')



cpuselect = Button(window, text = 'Pick a CPU', command = getdata)
cpuselect.grid(row = 7, columnspan = 2)

window.mainloop()
